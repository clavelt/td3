int max2(int a, int b) {
    int maxi = 0;
    if (a >= b) {
        maxi = a;
    }
    else{
        maxi = b;
    }
    return maxi;
}


int max3(int a, int b, int c){
    return (max2(max2(a,b),c));
}

int testmax(){
    Ut.afficherSL("Entrez la première valeur :");
    int a = Ut.saisirEntier();
    Ut.afficherSL("Entrez la deuxième valeur :");
    int b = Ut.saisirEntier();
    Ut.afficherSL("Le maximum des deux premiers nombres est :" + max2(a,b));
    Ut.afficherSL("Entrez la première valeur :");
    a = Ut.saisirEntier();
    Ut.afficherSL("Entrez la deuxième valeur :");
    b = Ut.saisirEntier();
    Ut.afficherSL("Entrez la dernière valeur :");
    int c = Ut.saisirEntier();
    Ut.afficherSL("Le maximum de tous les nombres est :" + max3(a,b,c));
    return max3(a,b,c);
}

void repetCarac (int nb, char car){
    //PR: Cet algorithme a besoin d'un nombre positif et d'un caractère
    //Action: Renvoie une pyramide de taille nb du caractère choisi
    for (int i = 0; i < nb; i++){
        Ut.afficher(car);
    }
}

void pyramideSimple(int h, char c){
    int nb = h;
    for (int i = 0; i < nb; i++){
        repetCarac(h-1,' ');
        repetCarac(i+1,c);
        repetCarac(i,c);
        Ut.sautLigne();
        h = h-1;
    }
}

void testPyramideSimple(){
    Ut.afficherSL("Entrez un entier :");
    int h = Ut.saisirEntier();
    Ut.afficherSL("Entrez un caractère :");
    char c = Ut.saisirCaractere();
    pyramideSimple(h,c);
}

void afficheNombresCroissants(int nb1, int nb2){
    int temp = 0;
    if (nb1 <= nb2){
        for (int i = nb1; i <= nb2; i++){
            temp = i % 10;
            Ut.afficher(temp);
            Ut.afficher(' ');
        }
    }
}

void afficheNombresDecroissants (int nb1, int nb2){
    int temp = 0;
    if (nb1 <= nb2){
        for (int i = nb2; i >= nb1; i--){
            temp = i % 10;
            Ut.afficher(temp);
            Ut.afficher(' ');
        }
    }
}

void pyramideElaboree(int h){
    int nb = h;
    int ligne = 0;
    for (int i = 1; i < nb+1; i++){
        repetCarac(h-1,' ');
        repetCarac(h-1,' ');
        afficheNombresCroissants(i,i+ligne);
        afficheNombresDecroissants(i,i+ligne -1);
        Ut.sautLigne();
        ligne = ligne + 1;
        h = h - 1;
    }
}

void testPyramideElaboree(){
    Ut.afficherSL("Entrez un entier :");
    int h = Ut.saisirEntier();
    pyramideElaboree(h);
}


int nbChiffres(float n ){
    int nbchiffre = 1;
    while (n>=10){
        n = n/10;
        nbchiffre += 1;
        }
    Ut.afficher(nbchiffre);
    return nbchiffre;
}

int nbChiffresDuCarre(float n){
    float temp = n*n;
    return nbChiffres(temp);
}

int sommediviseur(int a){
    int somme = 0;
    for (int i = 1; i < a; i++){
        if (a % i == 0){
            somme = somme + i;
        }
    }
    return somme;
}

Boolean nombreamis(int a, int b){
    if ((b == sommediviseur(a)) && (a == sommediviseur(b))){
        return true;
    }
    else{
        return false;
    }
}

void testAmis(int a){
    for (int i = 0; i < a; i++){
        for (int j = i+1; j < a; j++){
            if (nombreamis(i,j) == true){
                Ut.afficherSL(i + " et "+ j +" sont amis");
            }
        }
    }
}


int racineParfaite(int c ){
    for (int i = 0; i < c; i++){
        if (i*i == c){
            return i;
        }
    }
    return -1;
}

/**
 *
 * @param nb
 * @return vrai si un entier donné est un carré parfait, faux sinon.
 */

boolean estCarreParfait(int nb) {
    for (int i = 0; i < nb; i++) {
        if (i * i == nb) {
            return true;
        }
    }
    return false;
}

/**
 *
 * @param c1
 * @param c2
 * @return vrai si deux entiers donnés peuvent  être les côtés de l'angle droit d'un triangle rectangle dont les 3
 *        côtés sont des nombres entiers, faux sinon
 */

boolean  estTriangleRectangle (int c1, int c2){
    int temp = (c1*c1) + (c2*c2);
    if (Math.sqrt(temp) % 1 == 0){
        return true;
    }
    return false;
}



void nbTriangle(int n){
    double perimetre = 0;
    int compteur = 0;
    int i = 0;
    int j = i;
    int racine = 0;
    while (n> perimetre){
        i += 1;
        while (n>perimetre){
            if (Math.sqrt((i*i)+(j*j))%1 == 0){
                perimetre = i + j + (Math.sqrt((i*i)+(j*j)));
                compteur += 1;
            }
            if (perimetre < n){
                ;
            }
            j += 1;
        }
    }
    Ut.afficher(compteur);
}

void testsyracusien(int n){
    while (n != 1) {
        if (n % 2 == 0) {
            n = n / 2;
        }
        else {
            n = n * 3 + 1;
        }
    }
    Ut.afficherSL(n);
}

boolean testsyracusienV2(int n, int nbMaxOp){
    for (int i=0; i < nbMaxOp; i++){
        if (n%2 == 0) {
            n = n/2;
        }
        else {
            n = n * 3 + 1;
        }
    }
    if (n == 1){
        return true;
    }
    else {
        return false;
    }
}


void main(){
    testsyracusienV2(7, 3)
}